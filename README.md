# Skip The Dishes Coding Test #

### Coding test ###

The task is to create an application that accepts an outcode as a parameter. The application should then display the following information about each restaurant that delivers to that outcode by querying our API:
* Name.
* Rating.
* Types of food for the restaurant.

### User Stories ###

As a ​user running the application

I can v​iew a list of restaurants in a user submitted outcode (e.g. SE19)

So that ​I know which restaurants are currently available.

---

As a ​user running the application
I can v​iew the restaurant logo alongside restaurant information
So that ​I know exactly which restaurants are currently available.

Acceptance criteria:

● For the known outcode e​ c4m​, results are returned.

● The name, cuisine types and rating of the restaurant are displayed.


### To run the app on iPhone with single command ###
You can run:
### `yarn run-app`

## Available Scripts

To install the packages.
### `yarn install`

---

To install iOS pods.
### `cd ios && pod install`

---

To start the bundler.
### `yarn start`

---

To run the application in iOS.
### `yarn ios`

---

To run the application in android.
### `yarn android`

---

To test the application code.
### `yarn test`

---

To check the linting code error.
### `yarn lint`

---
To check your new typescript files.
### `yarn tsc`
