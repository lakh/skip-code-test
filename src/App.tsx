/**
 *
 * App.
 *
 * This is the entry file for the application, only setup basic modules here
 * like store configuration, navigation, theme provider.
 *
 */

import React from 'react';
import {SafeAreaView} from 'react-native';
import Home from './components/Home';

const App = () => {
  return (
    <SafeAreaView>
      <Home />
    </SafeAreaView>
  );
};

export default App;
