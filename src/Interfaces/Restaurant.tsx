/**
 *
 * Restaurant Interfaces.
 *
 * This class will have interfaces for restaurant, cuisines.
 *
 */

export interface Cuisine {
  Name: string;
}

export interface Restaurant {
  Id: Number;
  Name: string;
  LogoUrl: string;
  RatingAverage: string;
  CuisineTypes: [Cuisine];
}

export interface RestaurantData {
  Restaurants: [Restaurant];
}
