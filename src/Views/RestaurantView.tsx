/**
 *
 * Restaurant View.
 *
 * Render restaurant view to render in list view.
 *
 */

import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import Colors from '../helpers/Colors';
import {Restaurant} from '../Interfaces/Restaurant';

const RestaurantView: React.FC<Props> = (props) => {
  const {restaurant} = props;

  if (!restaurant) {
    return <Text style={styles.error}>Error in rendering restaurant</Text>;
  }

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={{uri: restaurant.LogoUrl}} />
      <Text style={styles.title}>{restaurant.Name}</Text>
      <View style={styles.ratingView}>
        <Text style={styles.rating}>{restaurant.RatingAverage}</Text>
      </View>
      <Text style={styles.cuisineType}>
        {restaurant.CuisineTypes.map((e) => e.Name).join(', ')}
      </Text>
    </View>
  );
};

// styles
const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '95%',
    margin: 5,
    padding: 15,
    borderColor: Colors.black,
    flex: 1,
    borderWidth: 1,
  },
  error: {
    margin: 20,
    fontSize: 25,
  },
  logo: {
    width: 80,
    height: 80,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  ratingView: {
    padding: 10,
    borderRadius: 50,
    backgroundColor: Colors.orange,
    alignSelf: 'flex-start',
    minWidth: 40,
    marginTop: 10,
    marginBottom: 10,
  },
  rating: {
    alignSelf: 'center',
    fontSize: 16,
    color: Colors.white,
  },
  cuisineType: {
    fontSize: 14,
  },
});

export interface Props {
  restaurant: Restaurant;
}

export default RestaurantView;
