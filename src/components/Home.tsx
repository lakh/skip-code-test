/**
 *
 * Home Page.
 *
 * Home page is to show restaurant listing with Name, Rating, Types of food for the restaurant.
 * Restaurents are fetching from API with user input postal code.
 *
 */

import React from 'react';
import {StyleSheet, View, TextInput, FlatList, Text} from 'react-native';
import RestaurantController from '../controllers/RestaurantController';
import Colors from '../helpers/Colors';
import RestaurantView from '../Views/RestaurantView';
import {RestaurantData} from '../Interfaces/Restaurant';

const Home: React.FC = () => {
  // Use text field in local state to save text field input.
  const [textField, setTextField] = React.useState<string>('');
  // Use postal code in local state to use for api call after user press enter in keyboard.
  const [postalCode, setPostalCode] = React.useState<string>('');
  // Use restaurant data in local state to keep data coming from api.
  const [restaurantsData, setRestaurantsData] = React.useState<
    RestaurantData | undefined
  >(undefined);
  // Use loading error in local state to show error if api fails.
  const [loadingError, setLoadingError] = React.useState<string>('');

  // Call api to get restaurants on change of postal code.
  React.useEffect(() => {
    setRestaurantsData(undefined);
    setLoadingError('');
    if (postalCode && postalCode.length > 0) {
      RestaurantController.getRestaurants(postalCode)
        .then((response) => {
          setRestaurantsData(response);
        })
        .catch((error) => {
          setLoadingError(error);
        });
    }
  }, [postalCode]);

  // Render Text Input to get postal code.
  const renderTextInput = (
    <View style={styles.inputView}>
      <TextInput
        defaultValue={textField}
        style={styles.input}
        placeholder="Search by postal code!"
        onChangeText={(text) => setTextField(text)}
        returnKeyType="search"
        clearButtonMode="while-editing"
        onSubmitEditing={() => setPostalCode(textField)}
      />
    </View>
  );

  // Check if restaurants are coming through API.
  const restaurantsAvailable =
    restaurantsData &&
    restaurantsData.Restaurants &&
    restaurantsData.Restaurants.length > 0;

  // Get status in string.
  const getStatus = () => {
    // If api returns any error.
    if (loadingError.length > 0) {
      return loadingError;
    }
    // If user enter postal code and api is running.
    if (postalCode && !restaurantsData) {
      return 'Loading...';
    }
    // Initial state to let user input postal code.
    if (!restaurantsData) {
      return 'Search restaurant by postal code.';
    }
    // If restaurants are not returning for postal code.
    if (!restaurantsAvailable) {
      return `No restaurants found in ${postalCode}`;
    }
  };

  // Render below condition if resstaurants are not available.
  if (!restaurantsData || !restaurantsAvailable) {
    return (
      <>
        {renderTextInput}
        <Text
          style={
            loadingError.length > 0 ? styles.errorText : styles.statusText
          }>
          {getStatus()}
        </Text>
      </>
    );
  }

  return (
    <FlatList
      ListHeaderComponent={renderTextInput}
      stickyHeaderIndices={[0]}
      data={restaurantsData.Restaurants}
      keyExtractor={(item) => item.Id.toString()}
      onEndReachedThreshold={0.5}
      numColumns={1}
      renderItem={({item}) => <RestaurantView restaurant={item} />}
      scrollEventThrottle={400}
    />
  );
};

// styles
const styles = StyleSheet.create({
  inputView: {
    backgroundColor: Colors.white,
    padding: 10,
  },
  input: {
    padding: 10,
    height: 40,
    borderColor: Colors.black,
    borderRadius: 10,
    borderWidth: 0.5,
  },
  statusText: {
    padding: 10,
    fontSize: 42,
  },
  errorText: {
    padding: 10,
    fontSize: 42,
    color: '#ff0000',
  },
});

export default Home;
