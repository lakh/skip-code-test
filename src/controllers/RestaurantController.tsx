/**
 *
 * RestaurantController.
 *
 * This controller calls and get response from api for restaurants by postal code.
 *
 */

import {AppURLs} from '../helpers/AppUrls';

class RestaurantController {
  restaurantPath: String;

  constructor() {
    this.restaurantPath = AppURLs.byPostCode;
  }

  getRestaurants = async (postalCode: String) => {
    try {
      const response = await fetch(`${this.restaurantPath}/${postalCode}`);
      const responseJson = await response.json();
      return Promise.resolve(responseJson);
    } catch (error) {
      return Promise.reject(error.message);
    }
  };
}

export default new RestaurantController();
