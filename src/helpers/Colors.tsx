export default {
  black: '#000000',
  dark: '#2B3460',
  orange: '#e3732d',
  white: '#FAFAFA',
};
